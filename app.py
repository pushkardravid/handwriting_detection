import os
from flask import Flask, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
import requests

import time

def getHandwrittenFromImage(f):
    subscription_key = "96225188456041c28f57bfbdd758fd2a"
    vision_base_url = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/"
    text_recognition_url = vision_base_url + "RecognizeText"
    image_path = f
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': subscription_key,
               "Content-Type": "application/octet-stream"}
    params = {'handwriting': True}
    response = requests.post(text_recognition_url,
                             headers=headers,
                             params=params,
                             data=image_data)
    operation_url = response.headers["Operation-Location"]

    analysis = {}
    while not "recognitionResult" in analysis:
        response_final = requests.get(operation_url, headers=headers)
        analysis = response_final.json()
        time.sleep(1)
    data = analysis['recognitionResult']['lines']
    text = ""
    lines = [d['text'] for d in data]
    return lines

UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = set(['jpeg','jpg','png'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/ml', methods=['GET', 'POST'])
def upload_file_ml():
    print "started"
    processed_text = ""
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            processed_text = getHandwrittenFromImage(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print processed_text
    return render_template('index.html',data_clean=processed_text,filename='',status="none")
if __name__ == '__main__':
    app.run(debug=True, port=5000)
